(ns fiverr-tomtcs-dataviewer.routes.home
  (:require [compojure.core :refer :all]
            [fiverr-tomtcs-dataviewer.views.layout :as layout]))

(defn home []
  (layout/common [:h1 "Hello World!"]))

(defroutes home-routes
  (GET "/" [] (home)))
